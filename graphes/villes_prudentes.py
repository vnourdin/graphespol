
import pandas as pd
import matplotlib.pyplot as plt

import data.villes_prudentes
import data.baac
import data.population

"""Les « villes prudentes » protègent-elles les vulnérables ?

Ce module permet de comparer le nombre de blessés graves vulnérables
(non-motorisés), rapporté à la population, entre les villes
labellisées « Ville Prudente » et les autres.

"""

class Graphe:

    """Attributs
    ---------
    annees = array
      Liste des annees de l'étude

    vulnerables : array
      Liste des catérogies simplifiées (voir data.baac) prises en
      compte pour compter les usagers vulnérables. Par défaut, on
      inclut les piétons (P), les cyclistes (V) et les EDP (EDP).

    gravite : array
      Liste des niveaux de gravité (voir data.baac) considérés dans le
      décompte des blessés. Par défaut, on inclut les morts (2) et les
      blessés hospitalisés (3).

    categorisation : array
      Liste des fichiers source de catégorisation des « Villes
      Prudentes » à prendre en compte. Par défaut, on utilise les
      communes répertoriées sur le site internet au 16/11/2021, plus
      les villes nouvellement intégrées au label, listées dans le
      communiqué de presse de novembre 2021.

    liste_prudentes : DataFrame
      Les villes prudentes lues à partir des sources sélectionnées.

    niv : DataFrame
      Niveaux de blessés par an et par 100.000 habitants, calculés
      chaque année pour chaque niveau de distinction.

    niv_global: DataFrame
      Niveaux de blessés par an et par 100.000 habitants, calculés sur
      toute la période considérée, pour chaque niveau de distinction.

    """
    
    annees = list(range(2015, 2021))
    vulnerables = ["P", "V", "EDP"]
    gravite = [2, 3]
    categorisation = ["2021-11-16-site.csv", "2021-11-CP.csv"]

    liste_prudentes = None
    niv = None
    niv_global = None

    def __init__(self, **params):
        """Initialisation

        Paramètres nommés
        -----------------
        annees, vulnerables, gravite, categorisation : voir les
        attributs de classe.

        """
        self.change_parametres(**params, force=True)

    def change_parametres(self, **params):
        """Changement des paramètres

        Change les paramètres, relit la liste des villes prudentes si
        nécessaire, et efface la DataFrame niv.

        Paramètres nommés
        -----------------
        annees, vulnerables, gravite, categorisation : voir les
        attributs de classe.

        force : boolean
          Utiliser force=True pour imposer la relecture de la liste
          des villes prudentes, même si categorisation n'est pas
          donné.

        """
        for k,v in params.items():
            if k in ['annees', 'vulnerables', 'gravite', 'categorisation']:
                setattr(self, k, v)
        # Si categorisation est fourni (ou avec le paramètre force),
        # relit la liste des villes prudentes avec leur catégorie.
        if params.get('force') or 'categorisation' in params:
            self.liste_prudentes = \
                data.villes_prudentes.liste(self.categorisation)
        # Efface les données qui ont été calculées dans niv.
        self.niv = None

    def regroupe_distinctions(self, regroupements):
        """Regroupement des catégories

        Regroupe des catégories de villes prudentes.

        Exemple : pour regrouper toutes les villes prudentes dans une
        seule catégorie 1, on pourra utiliser
        
        regroupe_distinctions({ 1: [1, 2, 3, 4, 5] })

        Paramètres
        ----------
        regroupements : dict
          Un dictionnaire avec pour clés les nouvelle valeurs de
          distinction, et pour valeurs des listes d'anciennes valeus
          de distinctions.

        """
        for dest, src in regroupements.items():
            # Modifie dans la base liste_prudentes les valeurs de
            # distinction selon la règle src→dest
            self.liste_prudentes.loc[
                self.liste_prudentes["distinction"].isin(src),
                "distinction" ] = dest
        self.niv = None

    def rajoute_distinction(self, df):
        """Rajoute une colonne distinction à une DataFrame

        Rajoute une colonne distinction donnant le niveau de ville
        prudente de chaque ligne, à partir d'une colonne INSEE qui
        identifie la ville.

        La valeur 0 sera affectée aux communes qui ne sont pas dans la
        liste des villes prudentes.

        Paramètres
        ----------
        df : DataFrame
          La DataFrame à lqeuelle ajouter le colonne distinction.

        """
        # Ajoute la colonne à partir des données chargées
        df = pd.merge(df,
                      self.liste_prudentes,
                      how="left", on="INSEE")
        # Pour les codes INSEE qui ne sont pas présents dans la liste
        # des villes prudentes, affecte la valeur 0
        df.loc[ df["distinction"].isnull(), "distinction" ] = 0
        return df

    def blesses_vulnerables_par_commune(self, annee):
        """Calcul du nombre de blessés vulnérables par commune

        Paramètres
        ----------
        annee : int
          L'année du calcul (le calcul se fait sur une seule année).

        Retour
        ------
        Une DataFrame, avec en particulier les colonnes suivantes :
        * INSEE, code INSEE d'une commune
        * n, nombre de blessés vulnérables dans cette commune
        * distinction, le niveau ville prudente de cette commune

        """

        # Lecture usagers :
        u = data.baac.lecture("usagers", annee=annee)

        # Sélection des blessés dont la gravité appartient aux valeurs
        # requises
        u = u[ u["grav"].isin(self.gravite) ]

        # Ajout des informations du véhicule depuis la base BAAC
        u = pd.merge(u, data.baac.lecture("vehicules", annee=annee),
                     how="left", on="id_vehicule")
        
        # Retrouve les noms de colonne initiaux pour les colonnes qui sont
        # doublonnées par le merge
        u.rename(columns={'Num_Acc_x':'Num_Acc',
                          'num_veh_x':'num_veh'},
                 inplace=True)

        # Classification des pietons (pour eux catvsimple contenait la
        # catégorie du véhicule qui l'a renversé)
        u.loc[ u["catu"] == 3, "catvsimple" ] = "P"
        
        # Sélection des victimes vulnérables
        victimes = u[ u["catvsimple"].isin(self.vulnerables) ]

        # Ajout de la table lieux du BAAC (pour avoir catr)
        victimes = pd.merge(victimes,
                            data.baac.lecture("lieux", annee=annee),
                            how="left", on="Num_Acc")

        # Sélection des accidents hors autoroute
        victimes = victimes[ victimes["catr"] != 1 ]

        # Ajout de la table caractéristiques du BAAC (pour avoir INSEE)
        victimes = pd.merge(victimes,
                            data.baac.lecture("caracteristiques", annee=annee),
                            how="left", on="Num_Acc")
        
        # Compte du nombre de lignes pour chaque commune
        comptes = victimes.groupby(["INSEE"])["INSEE"].count() \
                            .reset_index(name="n")
        
        # Ajout du niveau de labellisation ville prudente
        comptes = self.rajoute_distinction(comptes)

        return comptes

    def population_par_distinction(self, annee):
        """Calcul des populations totales par distinction

        Calcule la population totale de toutes les villes ayant un
        même niveau de labellisation Ville Prudente, pour chaque
        niveau.

        Paramètres
        ----------
        annee : int
          L'année du calcul (le calcul se fait sur une seule année).

        Retour
        ------
        Une DataFrame contenant les colonnes suivantes :
        * distinction
        * PMUN : la population totale.
        * n_communes : le nombre de communes concernées

        """
        # Lecture des populations
        pop = data.population.population_2018()

        # Ajout des niveaux de labellisation
        pop = self.rajoute_distinction(pop)

        # Calcul des sommes par niveau
        return pop.groupby(["distinction"]) \
                  .aggregate(pop=('PMUN','sum'),
                             n_communes=('distinction','count')
                             ).reset_index()

    def niveaux_par_distinction(self, annee):
        """Áccidentologie globale par niveau de labellisation

        Calcule, pour chque niveau de labellisation, le nombre de
        blessés vulnérables par 100.000 habitants.

        Paramètres
        ----------
        annee : int
          L'année du calcul (le calcul se fait sur une seule année).

        Retour
        ------
        Une DataFrame, avec les colonnes suivantes :
        * distinction
        * n, nombre de blessés vulnérables pour l'ensemble des
          communes de ce niveau de labellisation.
        * pop, la population totale.
        * niveau, le nombre de blessés vulnérables par 100.000
          habitants.
        * n_communes, le nombre de communes concernées.
        * communes_avec_blesses, le nombre de communes dans lesquelles
          on a observé au moins un blessé.
        * année, l'année concernée (la même pour toutes les lignes)

        """
        # Regroupe par niveau de labellisation les données qui ont été
        # calculées par commune
        comptes = self.blesses_vulnerables_par_commune(annee) \
                      .groupby(["distinction"]) \
                      .aggregate(n=('n','sum'),
                                 communes_avec_blesses=('n','count')
                                 ).reset_index()

        # D'un autre côté, calcule la population totale
        pop = self.population_par_distinction(annee)

        # Rejoint les deux tables
        comptes = pd.merge(comptes, pop, on="distinction")

        # Calcule le niveau : nombre de blessés par 100.000 habitants
        comptes["niveau"] = comptes["n"] / comptes["pop"] * 1e5

        # Rajoute l'année concernée
        comptes["annee"] = annee
        
        return comptes

    def calcul(self, force=False, calc_global=True):
        """Calcul des données utiles pour les graphes

        Si la DataFrame niv n'est pas présente (ou si force=True), la
        calcule à partir de niveaux_par_distinction appelée pour
        chaque année souhaitée.

        De plus, si calc_global=True, calcule niv_global

        Les colonnes de niv sont détaillées dans
        niveaux_par_distinction.

        Les colonnes de niv_global sont les suivantes :
        * distinction
        * n, le nombre de blessés sur toutes les communes de niveau de
          labellisation <distinction> et toute la période considérée.
        * pop, la population globale des communes considérées
        * n_communes, le nombre de communes correspondantes
        * niveau, le nombre de blessés par année et par 100.000
          habitants

        """
        if force or self.niv is None:

            # Part d'une DataFrame vide
            self.niv = pd.DataFrame()

            # et, pour chaque année…
            for a in self.annees:
                # … ajoute les données calculées par niveaux_par_distinction
                niv = self.niveaux_par_distinction(a)
                self.niv = self.niv.append(niv)

            if calc_global:

                # Regroupe par distinction, en faisant la somme de n,
                # et en gardant pop et n_communes
                x = self.niv.groupby(['distinction']) \
                            .aggregate(n=('n','sum'),
                                       pop=('pop','first'),
                                       n_communes=('n_communes','first')
                                       ).reset_index()

                # Calcule le niveau aggrégé
                x["niveau"] = x["n"] / x["pop"] * 1e5 / len(self.annees)

                self.niv_global = x

    def compare_distinctions(self, color=None):
        """Graphe de comparaison des niveaux de labellisation

        Compare, par un graphe, les niveaux de labellisation du point
        de vue du nombre de blessés vulnérables par 100.000 habitants
        et par année sur toute la période considérée.

        Paramètres
        ----------
        color : array
          Un eliste donnant les couleurs à utiliser pour les barres du
          graphique.

        """
        # Détermine les labels, qui seront formés par le niveau de
        # labellisation, et, sur une ligne au-dessous, du nombre de
        # villes concernées
        labels = [str(int(row["distinction"])) + "\n"
                  + "(n=" + str(int(row["n_communes"])) + ")"
                  for idx, row in self.niv_global.iterrows()]

        # Trace le barplot
        plt.bar(self.niv_global["distinction"],
                self.niv_global["niveau"],
                color=color,
                tick_label=labels)
        plt.ylabel("Blessés graves non motorisés par an pour 100.000 habitants")
                
    def evolution(self, labels={}):
        """Graphe d'évolution de l'accidentologie

        Compare, par un graphe, pour chaque niveau de labellisation,
        l'évolution du nombre de blessés vulnérables pour 100.000
        habitants au cours des années de la période d'observation.

        Paramètres
        ----------
        labels : dict
          Les labels à utiliser pour les niveaux de labellisation.

        """
        self.calcul()
        
        # Récolte la liste des niveaux de labellisation présents
        niveaux = sorted(self.niv["distinction"].unique())

        for d in niveaux:
            # Extrait les données d'accidentologie pour le niveau d
            x = self.niv[ self.niv["distinction"]==d ].sort_values('annee')

            # Détermine le label à utiliser pour ce niveau de
            # labellisation à partir du paramètre
            l = labels.get(d)
            if l is None:
                # Si aucune valeur n'est fournie, essaye de trouver
                # une valeur raisonnable
                if d==0:
                    l = "Non labellisées"
                elif d==1:
                    if max(niveaux)==1:
                        l = "Labellisées"
                    else:
                        l = "1 cœur"
                else:
                    l = f"{int(d)} cœurs"

            # Tracé pour la catégorie de labellisation d
            plt.plot(x["annee"], x["niveau"], label=l)

        plt.xticks(self.annees)
        plt.ylabel("Blessés graves non motorisés pour 100.000 habitants")
        plt.legend()

    def data(self, fichier):
        """Données relatives à l'infographie de Pol GM

        Enregistre les résultats des calculs dans des fichiers CSV.

        Les paramètres ici utilisés sont les suivants :

        * Période d'observation : 2015 à 2020

        * Catégories villes prudente à partir de l'affichage sur le
          site https:www.villeprudente.fr au 13/11/2021 (date de
          production de cette infographie).

        _distinction.csv pour la comparaison des niveaux de
        labellisation « ville prudente », globalement sur les 6 années
        observées.

        _evolution.csv pour l´evolution au cours des 6 années
        observées, pour d'un côté les villes non-labellisées et de
        l'autre les villes labellisées.

        Paramètres
        ----------
        fichier : str
            La base du chemin des fichiers où enregistrer les graphes.

        """
        self.change_parametres(annees=[2015, 2016, 2017, 2018, 2019, 2020],
                               categorisation=["2021-11-13-site.csv"])
        self.calcul()
        self.niv_global.to_csv(fichier + "_distinctions.csv")
        self.regroupe_distinctions({1:[1, 2, 3, 4, 5]})
        self.calcul(calc_global=False)
        self.niv.to_csv(fichier + "_evolution.csv")
        
    def graphe(self, fichier):
        """Graphes utilisés dans l'infographie de Pol GM

        (Voir data)

        Paramètres
        ----------
        fichier : str
            La base du chemin des fichiers où enregistrer les graphes.

        """
        plt.close('all')
        self.compare_distinctions(color=['#ff6600','#54ddff','#00ccff',
                                         '#01aad5','#90aab2'])
        plt.savefig(fichier + "_distinctions.png")
        plt.close('all')
        self.evolution()
        plt.savefig(fichier + "_evolution.png")
    
