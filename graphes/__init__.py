
import importlib
import os
from pathlib import Path


def fabrique(nom, **params):
    """Construction d'un jeu de graphes

    Cette fonction permet de construire un jeu de graphes relatifs à
    un sujet donné, en suivant les étapes suivantes :

    * téléchargement éventuel des bases de données publiques nécessaires

    * calculs

    * enregistrement des résultats, et production éventuelle de
      graphes pouvant servir de base à une analyse.

    Paramètres
    ----------
    nom : str
        Le nom du sujet concerné, qui correspond à un module du même nom dans
        le répertoire graphes.

    Résultats
    ---------
    Les résultats (fichiers CSV, TXT ou images) sont stockés dans le
    répertoire portant le nom du sujet, à l'intérieur du répertoire resultats.
    """
    module = importlib.import_module("graphes." + nom)
    repertoire = os.path.join(Path(os.path.dirname(__file__)).parent,
                              "resultats", nom)
    # Création du répertoire de résultats s'il n'existe pas déjà
    if not os.path.isdir(repertoire):
        os.mkdir(repertoire)

    # Utilisation de la classe Graphe dans le module correspondant au
    # nom du sujet.
    g = module.Graphe(**params)
    g.data(os.path.join(repertoire, "data"))
    g.graphe(os.path.join(repertoire, "figure"))
