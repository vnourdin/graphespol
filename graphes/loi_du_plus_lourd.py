
import data.baac as baac
import matplotlib.pyplot as plt
import math

"""Qui tue qui ?

Ce module permet de recenser les décès, en fonction des catégories du
véhicule de la personne décédée et de l'autre véhicule impliqué dans
l'accident.

"""

class Graphe:

    """Attributs
    ---------
    annee : l'année de l'étude

    faceaface: une DataFrame qui contient, pour chaque couple
        (catvsimple, face_catvsimple), le nombre de décès
        concernés. catvsimple est la catégorie du véhicule de la
        personne décédée (ou "P" si c'est un piéton), et
        face_catvsimple la catégorie de l'autre véhicule impliqué dans
        l'accident, ou "0" s'il n'y en a aucun, ou "MULT" s'il y en a
        plusieurs autres de catégories différentes.

    """

    annee = 2020

    def __init__(self, annee=2020):
        """Paramètres
        ----------
        annee : integer
            L'année de l'analyse.

        """
        self.annee = annee
        self.calcule()

    def calcule(self):
        """Calculs

        Lecture des bases de données et calculs (DataFrame faceaface).
        """
        vic = baac.victimes_face(annee=self.annee)
        self.faceaface = vic[vic["grav"]==2] \
            .groupby(['catvsimple','face_catvsimple'])['catvsimple'] \
            .count().reset_index(name="n")

    def data(self, fichier):
        """Sauvegarde des données dans un fichier CSV

        Paramètres
        ----------
        fichier : str
            La base du chemin du fichier à enregistrer.
        """
        self.faceaface.to_csv(fichier + '.csv', index=False)

    def nb(self, cat, facecat):
        """Nombre de morts pour un couple catvsimple/face_catvsimple donné

        Paramètres
        ----------
        cat : str
            La catégorie des personnes décédées.

        facecat : str
            La catégorie des autres véhicules  impliqués.

        Retour
        ------
        Le nombre de décès correspondants à ces circonstances.

        """
        return self.faceaface.loc[(self.faceaface["catvsimple"]==cat)&
                                  (self.faceaface["face_catvsimple"]==facecat),
                                  "n"] \
                         .values.sum()

    def dessine_cercles(self, taillemax=1.5, size=0.4,
                        coul_lignes='#009DE360', coul_cercles='#E30000'):
        """Graphe des cercles des morts

        Dessine des cercles dont la surface est proportionnelle au
        nombre de morts pour chaque couple catvsimple/face_catvsimple.

        Paramètres
        ----------
        taillemax : float
            Le diamètre du cercle le plus grand, étant donné que deux
            cercles voisins sont distants de 1.

        size : float
            Distance entre deux cercles en pouces (inches).

        coul_lignes : str
            Couleur des lignes à dessiner.

        coul_cercles : str
            Couleur des cercles à dessiner.

        """

        plt.close('all')
        plt.axis('off')
        plt.rc('font', size=8)
        
        nmax = max(self.faceaface["n"])
        cats = list(self.faceaface["catvsimple"].unique())
        visavis = list(self.faceaface["face_catvsimple"].unique())

        xmin, xmax = -2, (len(visavis)-1)+0.6
        ymin, ymax = -(len(cats)-1)-0.6, 1.5

        for i,facecat in enumerate(visavis):
            line = plt.Line2D((i,i), (0.5, ymin),
                              lw=1, color=coul_lignes)
            plt.gca().add_line(line)
            
        for j,cat in enumerate(cats):
            line = plt.Line2D((-0.5, xmax), (-j,-j),
                              lw=1, color=coul_lignes)
            plt.gca().add_line(line)

        plt.text( xmin, -(len(cats)-1)/2, "Décès",
                  rotation = 90,
                  horizontalalignment='left',
                  verticalalignment='center' )

        plt.text( (len(visavis)-1)/2, ymax, "Véhicule percuté",
                  horizontalalignment='center',
                  verticalalignment='top' )

        for i,facecat in enumerate(visavis):
            plt.text( i, 1, facecat,
                      horizontalalignment='center',
                      verticalalignment='top' )
            for j,cat in enumerate(cats):
                n = self.nb(cat,facecat)
                c = plt.Circle( (i,-j),
                                radius=0.5*taillemax*math.sqrt(n/nmax),
                                linewidth=0,
                                color=coul_cercles, zorder=5)
                plt.gca().add_patch(c)
                if n>0:
                    plt.text( i + (0.1 if n<10 else 0),
                              -j-0.3, str(n),
                              horizontalalignment='center',
                              verticalalignment='center',
                              zorder=10)
        for j,cat in enumerate(cats):
            plt.text( -1.5, -j, cat,
                      horizontalalignment='left',
                      verticalalignment='center' )

        plt.xlim(xmin, xmax)
        plt.ylim(ymin, ymax)

        plt.gcf().set_size_inches(size*(xmax-xmin), size*(ymax-ymin))
        plt.subplots_adjust(left=0, bottom=0, right=1, top=1)

    def graphe(self, fichier):
        """Production et enregistrement du graphe

        Paramètres
        ----------
        fichier : str
            La base du chemin des fichiers où enregistrer les graphes.

        """
        self.dessine_cercles()
        plt.savefig(fichier + '_cercles.svg', transparent=True)

