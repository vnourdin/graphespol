
import pandas as pd
from data import Sources
from data.population import insee_date

"""Villes prudentes

Ce module permet d'intégrer la liste des villes classées comme « ville
prudente » par l'association « Prévention Routière ».

Cette liste a été visible en novembre 2021 à l'adresse
https://www.villeprudente.fr/les-villes-et-villages-prudents/

En novembre 2021, de nouvelles communes ont été intégrées au label
(voir communiqué de presse alors accessible à l'adresse
https://www.preventionroutiere.asso.fr/wp-content/uploads/2021/11/CP_VillePrudente_17.11.-2021.pdf
mais maintenant inaccessible), mais elles n'ont pas intégré la liste
sur le site.

Début 2022, la liste des communes labellisées a disparu du site
https://www.villeprudente.fr/, et n'est plus visible au 29/01/2022.

Les données regroupées ici ne sont donc plus publiques à ce jour.

Le fichier 2021-11-04-site.csv contient la liste des « villes
prudentes » visibles sur le site internet au 04/11/2021.

Le fichier 2021-11-13-site.csv contient la liste des « villes
prudentes » visibles sur le site internet au 13/11/2021.

Le fichier 2021-11-16-site.csv contient la liste des « villes
prudentes » visibles sur le site internet au 16/11/2021.

Le fichier 2021-11-CP.csv contient la liste des « villes prudentes »
issues du communiqué de presse de novembre 2021, exception faite de
Saint-Maur (Val de Loire) et de Prades (Occitanie), qui n'ont pas pu
être identifiées (plusieurs communes du même nom existent dans ces
régions).

"""

sources = Sources(__name__)

sources.enregistre("2021-11-04-site.csv",
  {"type":"local",
   "src": [
       "https://www.villeprudente.fr/les-villes-et-villages-prudents/"
   ]})

sources.enregistre("2021-11-CP.csv",
  {"type":"local",
   "src": [
       "https://www.preventionroutiere.asso.fr/wp-content/uploads/2021/11/CP_VillePrudente_17.11.-2021.pdf"
   ]})

def liste(fichiers=["2021-11-16-site.csv", "2021-11-CP.csv"]):
    """Liste des villes prudentes

    Fabrique la liste des villes prudentes

    Retour
    ------
    Une DataFrame, avec principalement les colonnes INSEE (code INSEE
    de la commune) et distinction (entier donnant le nombre de cœurs
    du label Ville Prudente de la commune).

    """
    x = pd.DataFrame()
    for f in fichiers:
        x = x.append(pd.read_csv(
            sources.fichier_joint(f),
            dtype={'INSEE':str, 'distinction':int}, sep=","))

    # Retire les informations doubles (en ne gardant que la dernière)
    x.drop_duplicates(subset=['INSEE'], keep='last')

    return x
