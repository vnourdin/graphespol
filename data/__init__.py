
import os
import requests
import zipfile

class Sources:
    """Une classe pour organiser une sources de données publiques

    Cette classe permet de gérer le téléchargement de fichiers de
    données publiques relatifs à une source de données ayant son
    module dans le répertoire data.
    """
    module = None
    url = {}

    def __init__(self, module):
        """
        Paramètres
        ----------
        module : str
            Donne le nom du module utilisant cette source de donnée,
            qui servira de nom de répertoire dans lequel stocker les
            fichiers téléchargés
        """
        self.module = module.replace("data.", "")
    
    def enregistre(self, fichier, source):
        """Enregistre l'URL où l'on peut trouver un fichier de données

        Paramètres
        ----------
        fichier : str
            Le nom du fichier de données qui sera stocké en local.

        source :
            La source où trouver ce fichier, qui peut être de
            différents types :

            * une URL à laquelle on peut accéder au fichier.

            * un dictionnaire de la forme renseignant les clés type,
              src et fichier.
              Dans le cas où type="zip", src donne le nom du fichier
              zip (qui peut être lui-même enregistré comme accessible
              à partir d'une URL par exemple), et fichier le nom du
              fichier à extraire de cette archive zip.

        """
        self.url[fichier] = source

    def fichier_joint(self, fichier):
        """Donne le chemin où trouver le fichier de données en local

        Cette méthode permet de retrouver le chemin complet du fichier
        enregistré en local. Si besoin, ce fichier est d'abord
        téléchargé depuis l'URL prévue.

        Paramètres
        ----------
        fichier : str
            Nom du fichier de données.
        """
        directory = os.path.join(os.path.dirname(__file__),
                                 self.module)
        path = os.path.join(directory, fichier)

        if not os.path.isfile(path):
            u = self.url.get(fichier)
            if u is None:
                raise ValueError(f"Source de données introuvable : {fichier}")
            if type(u) is str:

                # La source est une chaîne de caractères : c'est donc
                # l'URL à laquelle télécharger le fichier
                
                print(f"Téléchargement du fichier {fichier} depuis {u}…")
                response = requests.get(u)
                open(path, "wb").write(response.content)

            elif type(u) is dict:

                # La source est sous la forme d'un dictionnaire : on
                # regarde quel est la valeur de "type":
                
                t = u.get("type")
                if t == "zip":
                    # Dans le cas "type":"zip", on en extrait le
                    # fichier requis
                    with zipfile.ZipFile(self.fichier_joint(u["src"]), 'r') \
                         as zipf:
                        print("Extraction…")
                        zipf.extract(u["fichier"], directory)
                        if u["fichier"] != fichier:
                            os.rename(os.path.join(directory, u["fichier"]),
                                      path)
                else:
                    raise ValueError(f"Type de source inconnu : type={t}")
            else:
                raise ValueError("Type de source inconnu : " + str(type(u)))
            
        return path
