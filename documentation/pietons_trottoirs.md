# Piétons renversés sur un trottoir en fonction du véhicule

![passagepieton](../infographies/trottoirs.png)

Ce module permet de calculer le nombre de piétons blessés sur des
trottoirs, en fonction du type de véhicule qui les a percuté.

Vous pouvez fabriquer un graphe similaire à celui représenté sur
l'infographie (et les données correspondantes) avec les commandes
`python3` suivantes :

```python
import graphes
graphes.fabrique("pietons",
  annees=[2015,2016,2017,2018,2019,2020],
  regroupe_categories={ 'O' : ('X', 'S', '3RM', '4RM', 'TR'),
                        'PL/TC' : ('PL', 'TC') },
  locp=[5])
```

Vous retrouverez alors le résultat dans le répertoire
`resultats/pietons`.

La description des options du graphe `pietons` peut être retrouvée sur
la page des [passages protégés](pietons_passages_proteges.md).
