# Piétons renversés sur un passage protégé en fonction du véhicule

![passagepieton](../infographies/passage_pieton2_v6.png)

Ce module permet de calculer le nombre de piétons blessés sur des
passages protégés, en fonction du type de véhicule qui les a percuté.

Vous pouvez fabriquer un graphe similaire à celui représenté sur
l'infographie (et les données correspondantes) avec les commandes
`python3` suivantes :

```python
import graphes
graphes.fabrique("pietons")
```

Vous retrouverez alors le résultat dans le répertoire
`resultats/pietons`.

Vous pouvez adapter ces résultats à d'autres contextes, grâce aux
options suivantes. En guise d'exemple, si vous ne vous intéressez qu'à
la Bretagne (départements 22, 29, 35 et 56) sur les années 2018 à
2020, utilisez

```python
import graphes
graphes.fabrique("pietons", region=['22','29','35','56'], annees=[2018, 2019, 2020])
```


* `annees` : liste des années de l'étude. La valeur par défaut est
  `[2019]`, mais vous pouvez l'étendre en utilisant par exemple
  `[2015,2016,2017,2018,2019,2020]`.

* `region` : une région dans laquelle restreindre l'étude, sous la
  forme d'une liste de débuts de codes INSEE des communes à
  considérer. La valeur par défaut (`None`) sélectionne la France
  entière. Pour choisir de se restreindre aux communes du Rhône (y
  compris la métropole de Lyon), on peut choisir `['69']`. Pour l'Île
  de France, `['75','77','78','91','92','93','94','95']`.

* `regroupe_categories` : un dictionnaire décrivant les regroupements
  de catégories à effectuer. La valeur par défaut est
  ```python
  { 'O' : ('X', 'S', '3RM', '4RM', 'TR', 'EDP'),
    'PL/TC' : ('PL', 'TC') }
  ```

  Ainsi, `O` représentera les engins spéciaux et tracteurs agricoles
  (`S`), les trois et quatre roues motrices (`3RM` et `4RM`), les
  trains et tramways (`TR`) et les EDP et EDPM (`EDP`), ainsi que les
  « autres véhicules » (`X`). De même `PL/TC` regroupe les poids
  lourds (`PL`) et les bus et autobus (`TC`).

  Vous pouvez également modifier les regroupements de catégories de
  manière plus fine en éditant le fichier
  [catvsimple.csv](../data/baac/catvsimple.csv).
  
* `locp` : la liste des codes de localisation du piéton considérés. La
  valeur par défaut `[3,4]` correspond aux passages piétons (sans et
  avec signalisation lumineuse). La valeur `[5]` correspond aux
  trottoirs (voir un [exemple](pietons_trottoirs.md)).

* `unite` : le nombre de blessés qui correspondent à un cercle complet
  dans le graphique. La valeur par défaut est 10.

* `hauteur` : le nombre de cercles dans une colonne du graphique. La
  valeur par défaut est 10.
