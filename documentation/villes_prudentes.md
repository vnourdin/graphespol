# Les villes labellisées « Ville Prudente » plus dangereuses pour les piétons et cyclistes

![villeprudente](../infographies/labelVillesPrudentes_v5.png)

Ce module permet de comparer le nombre de blessés graves (hospitalisés
et décédés) piétons et cyclistes provoqués par des accidents de la
route dans les communes de France labellisées « Ville Prudente » par
l'association « Prévention Routière », rapporté au nombre d'habitants,
à la même statistique pour les villes non labellisées.

Vous pouvez fabriquer un graphe similaire à celui représenté sur
l'infographie (et les données correspondantes) avec les commandes
`python3` suivantes :

```python
import graphes
graphes.fabrique("villes_prudentes")
```

Vous retrouverez alors le résultat dans le répertoire
`resultats/villes_prudentes`.

## Données

Les données d'accidentologie proviennent du BAAC. Les populations
utilisées sont celles recenssées par l'INSEE en 2018.

La liste des villes labellisées « Ville Prudente » par l'association
« Prévention Routière » a été relevée à plusieurs moments en novembre
2021, plusieurs fois différente. Elle a disparu du site en janvier
2022. Un communiqué de presse de novembre 2021 mentionne également
certaines communes nouvellement labellisées. Il a maintenant disparu
du site https://www.villeprudente.fr/, mais une
[copie](../data/villes_prudentes/CP_VillePrudente_17.11.-2021.pdf)
peut être trouvée dans le répertoire `data/villes_prudentes`.  La
liste retenue pour cette analyse est celle relevée le 16/11/2021 (date
de la publication de l'infographie) sur le site
https://www.villeprudente.fr/, à laquelle ont été ajoutées les
communes du communiqué de presse.
