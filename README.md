# Graphiques sécurité routière

Ce dépôt contient les codes permettant de créer les graphes contenus
dans certaines infographies de Pol Grasland-Mongrain
([@PolGM1](https://twitter.com/PolGM1/) sur Twitter) à partir des
bases de données publiques.

## Dépendances

Pour pouvoir exécuter ces codes, vous aurez besoin de `python3`,
accompagné des modules suivants : `pandas`, `statsmodels`,
`matplotlib`, `requests`.

## Exécution

Pour fabriquer les données et graphiques correspondant à une
infographie, lancez `python3` dans le répertoire de ce projet, et
utilisez les commandes suivantes :

```python
import graphes
graphes.fabrique("pietons")
```

En remplaçant `pietons` par le graphique que vous voulez reproduire
(cf **Détails** ci-dessous).

Vous retrouverez alors le résultat dans un sous-dossier spécifique du
répertoire `resultats`.

## Licence

Les codes contenus dans ce dépôt sont distribués par Jojo et Pol
Grasland-Mongrain sous la licence
[GPLv3+](https://www.gnu.org/licenses/).

Les infographies contenues dans le répertoire `infographies` sont
distribuées par Pol Grasland-Mongrain sous la licence [CC
BY-SA](https://creativecommons.org/licenses/by-sa/4.0/).

## Détails et options

### Piétons renversés sur un trottoir en fonction du véhicule

![trottoir](infographies/trottoirs.png)
Publié le [14/02/2022](https://twitter.com/PolGM1/status/1493172393812140034).
[**Détails**](documentation/pietons_trottoirs.md)

### Piétons renversés sur un passage piéton en fonction du véhicule

![passagepieton](infographies/passage_pieton2_v6.png)
Publié le [05/02/2022](https://twitter.com/PolGM1/status/1489965194470117376).
[**Détails**](documentation/pietons_passages_proteges.md)

### Les villes labellisées « Ville Prudente » plus dangereuses pour les piétons et cyclistes

![villeprudente](infographies/labelVillesPrudentes_v5.png)
Publié le [16/11/2021](https://twitter.com/PolGM1/status/1460507493021503493).
[**Détails**](documentation/villes_prudentes.md)
