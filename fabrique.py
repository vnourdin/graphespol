#!/usr/bin/python

import sys
import os

base = os.path.dirname(__file__)

sys.path.append(base)

import graphes

for m in sys.argv:
    if os.path.isfile(f"{base}/graphes/{m}.py"):
        print(f"Graphes de {m}…")
        graphes.fabrique(m)

